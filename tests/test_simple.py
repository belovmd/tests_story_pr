import unittest

import simple


class TestSimple(unittest.TestCase):

    def test_one_case_10(self):
        result = simple.check_is_simple(10)
        self.assertFalse(result)
