import math


def check_is_simple(num):
    """Checks if number is simple"""
    for divider in range(2, int(math.floor(num**0.5))):
        if not (num % divider):
            return False

    return True
